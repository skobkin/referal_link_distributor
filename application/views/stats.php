<h4>Статистика</h4>

<table width="98%" border="1" class="table table-bordered">
	<thead>
		<th class="span1">&nbsp;</th>
		<th class="span9">Источник</th>
		<th class="span2">Время</th>
	</thead>

	<tbody>
<?php	$count = 0;
		if (isset($items[0]))
		foreach ($items as $key => $item) :
		$count++; ?>
		<tr>
			<td><?php echo $key + 1; ?></td>
			<td><a href="<?php $ref = substr($item['referer'], 0, strrpos($item['referer'], '/') + 1); echo $ref; ?>" target="_blank"><?php echo $ref; ?></a></td>
			<td><?php echo $item['time']; ?></td>
		</tr>
<?php	endforeach; ?>

		<tr class="summary">
			<td>Итого:</td>
			<td colspan="2"><?php echo $count; ?></td>
		</tr>
	</tbody>
</table>