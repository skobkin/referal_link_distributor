<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo $this->config->item('site_title');?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Skobkin.ru">

		<!-- Le styles -->
		<link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet">
		<style>
			body {
			padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
			}
		</style>
		<link href="<?php echo base_url('css/bootstrap-responsive.min.css'); ?>" rel="stylesheet">

		<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>
		<script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>

		<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

	</head>

	<body>

		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</a>
					<a class="brand" href="<?php echo base_url(); ?>"><b><?php echo $this->config->item('site_heading'); ?></b></a>
					<div class="nav-collapse">
						<ul class="nav">
							<li <?php if ($this->uri->segment(1) == '') echo 'class="active"' ?>><a href="<?php echo base_url(); ?>"><i class="icon-home icon-white"></i> <strong>Главная</strong></a></li>
<?php					if ($this->session->userdata('logged_in') == 1) : ?>
							<li <?php if ($this->uri->segment(2) == 'stats') echo 'class="active"' ?>><a href="<?php echo site_url('user/stats'); ?>"><i class="icon-info-sign icon-white"></i> <strong>Статистика</strong></a></li>
<?php					endif; ?>
						</ul>
<?php					if ($this->session->userdata('logged_in') != 1) : ?>
						<form method="POST" action="<?php echo site_url('user/login'); ?>" class="navbar-form pull-right">
							<input name="login" type="text" class="span2 verify" placeholder="логин" pattern="[a-zA-Z0-9_-]{2,32}">
							<input name="password" type="password" class="span2 verify" placeholder="пароль" pattern="[a-zA-Z0-9_-]{<?php echo $this->config->item('password_length') ?>}">
							<button type="submit" class="btn">Вход</button>
						</form>
<?php					else: ?>
						<ul class="nav pull-right">
							<li><a href="<?php echo site_url('user/logout'); ?>"><i class="icon-off icon-white"></i> <strong>Выход</strong></a></li>
						</ul>
<?php					endif; ?>
					</div><!--/.nav-collapse -->

				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="span12">
