					<div class="row">
<?php					if ($this->session->userdata('logged_in') != 1) : ?>
						<div class="span6">
							<h3>Пользователь Dropbox?</h3>
							<h5>Зарегистрируйся в системе и прокачай аккаунт!</h5>
							<form class="form-horizontal" method="POST" action="<?php echo site_url('user/register'); ?>">
								<div class="control-group">
									<label class="control-label" for="inputLogin">Логин в системе</label>
									<div class="controls">
										<input name="login" type="text" id="inputLogin" class="verify" placeholder="login" pattern="[a-zA-Z0-9_-]{2,32}">
									</div>
								</div>

								<div class="control-group">
									<label class="control-label" for="inputLink">Ваша ссылка</label>
									<div class="controls">
										<input name="link" type="text" id="inputLink" class="verify" placeholder="http://db.tt/xxxxxxx" pattern="http\:\/\/db\.tt\/\w{5,}">
									</div>
								</div>

								<div class="control-group">
									<label class="control-label" for="inputTarget">Нужно рефералов</label>
									<div class="controls">
										<input name="target" type="number" min="1" max="32" id="inputTarget" value="1">
									</div>
								</div>

								<div class="control-group">
									<div class="controls">
										<button type="submit" class="btn btn-large btn-primary">Зарегистрироваться</button>
									</div>
								</div>
							</form>
						</div>

						<div class="span6">
							<h3>Еще нет аккаунта Dropbox?</h3>
							<h5>Зарегистрируйся как реферал и получи 500 МБ бонусом!</h5>

							<a href="<?php echo site_url('referer'); ?>" class="btn btn-large btn-inverse">Стать рефералом</a>
						</div>
<?php					else: ?>
						<div class="span12">
							<div class="hero-unit">
								<h2>Привет, <?php echo $this->session->userdata('login'); ?></h2>
								<p>Ты уже зарегистрирован и можешь посмотреть статистику переходов по своей ссылке</p>
								<p>
									<a href="<?php echo site_url('user/stats'); ?>" class="btn btn-primary btn-large">
										Смотреть статистику
									</a>
								</p>
							</div>
						</div>
<?php					endif; ?>
					</div>