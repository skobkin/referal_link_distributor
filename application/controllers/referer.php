<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referer extends CI_Controller {

	public function index()
	{
		// Loading model
		$this->load->model('users', '', TRUE);
		$this->load->model('log', '', TRUE);

		// Loading user agent class
		$this->load->library('user_agent');

		if ( ! $user = $this->users->get_minimal())
			redirect($this->config->item('default_redirect'));

		// Add 1 to redirects number
		$this->users->plus_redirect($user[0]['id']);

		// Writing log
		$this->log->add(
				$user[0]['id'],
				($this->agent->is_referral()) ? $this->agent->referrer() : '',
				$this->input->ip_address(), // ip
				$this->agent->agent_string() // user agent string
				);

		// Redirecting client to user ref link
		redirect($user[0]['link']);
	}
}