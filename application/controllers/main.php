<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		// Сессии
		$this->load->library('session');

		// Профайлер
		$this->output->enable_profiler($this->config->item('enable_profiler'));

		$this->load->view('header');
		$this->load->view('main');
		$this->load->view('footer');
	}
}