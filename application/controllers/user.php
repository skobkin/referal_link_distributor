<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// Sessions
		$this->load->library('session');
	}


	// Method for registration form
	public function register()
	{
		// Checking login state
		if ($this->session->userdata('logged_in') == 1)
			redirect(site_url('user/stats'));

		// Profiler
		$this->output->enable_profiler($this->config->item('enable_profiler'));



		// Checking data
		if ((preg_match('/^[a-zA-Z0-9_-]{2,32}$/', $this->input->post('login')) === 1)
				&& (preg_match('/^http\:\/\/db\.tt\/\w{5,}$/', $this->input->post('link')) === 1)
				&& (($this->input->post('target') >= 1) && ($this->input->post('target') <= 32)))
		{
			// Loading text helper
			$this->load->helper('string');

			//Loading header
			$this->load->view('header');

			$data['password'] = random_string('alnum', $this->config->item('password_length'));
			$data['login'] = $this->input->post('login');

			// Loading users model
			$this->load->model('users', '', TRUE);
			// Registering user
			if ( ! $user_id = $this->users->add($this->input->post('login'), md5($data['password']), $this->input->post('link'), $this->input->post('target')))
				show_error('Ошибка регистрации', 500);

			// Writing session
			$session = array(
				'id' => $user_id,
				'login' => $this->input->post('login'),
				'logged_in' => 1
			);
			$this->session->set_userdata($session);


			$this->load->view('reg_success', $data);

			// Loading footer
			$this->load->view('footer');
		}
		else
			show_error('Неверный логин или ссылка', 400);
	}

	// Method for login form
	public function login()
	{
		if ($this->session->userdata('logged_in') == 1)
			   redirect(site_url('user/stats'));

		if($this->input->post('login') && $this->input->post('password'))
		{
			$this->load->model('users', '', TRUE);
			if ($user = $this->users->check($this->input->post('login'), md5($this->input->post('password'))))
			{
					$session = array(
							'id' => $user[0]['id'],
							'login' => $user[0]['login'],
							'logged_in' => 1
							);
					$this->session->set_userdata($session);

					redirect(site_url('user/stats'));
			}
			else
				show_error('Авторизация не удалась', 401);
		}
		else
			show_error('Недостаточно данных', 400);
	}

	// Method for logout
	public function logout()
	{
		if ($this->session->userdata('logged_in') == 1)
			$this->session->sess_destroy();

		redirect (base_url());
	}

	// Method for user stats
	public function stats()
	{
		// Checking login state
		if ($this->session->userdata('logged_in') != 1)
			redirect(base_url());

		// Profiler
		$this->output->enable_profiler($this->config->item('enable_profiler'));

		//Loading header
		$this->load->view('header');

		$this->load->model('log', '', TRUE);

		$data['items'] = $this->log->get_by_user($this->session->userdata('id'));

		$this->load->view('stats', $data);

		//Loading header
		$this->load->view('footer');
	}
}