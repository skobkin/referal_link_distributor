<?php

class Users extends CI_Model {

	var $id = '';
	var $login = '';
	var $password = '';
	var $link = '';
	var $redirects = '';
	var $target = '';

	// Adding new user
	function add($login, $password, $link, $target)
	{
		$this->login = $login;
		$this->password = $password;
		$this->link = $link;
		$this->target = $target;

		$this->db->insert('users', $this);
		return $this->db->insert_id();
	}

	// Проверка пользователя
	function check($login, $password) {
		$query = $this->db->where(array(
			'login' =>  $login,
			'password' => $password
		))
				->get('users');
		$result = $query->result_array();
		if (isset($result[0]))
			return $result;
		else
			return FALSE;
	}

	// Search user with minimum of redirects
	function get_minimal()
	{
		$query = $this->db->query('
			SELECT * FROM `users`
			JOIN (SELECT MIN(`redirects`) as minimal FROM `users`) t2 ON `users`.`redirects` = `t2`.`minimal`
			WHERE `users`.`redirects` < `users`.`target`
			LIMIT 1');

		$result = $query->result_array();
		if (isset($result[0]))
			return $result;
		else
			return FALSE;
	}

	// Update redirects count
	function plus_redirect($user_id)
	{
		$this->db->where('id', $user_id)
				->set('redirects', 'redirects + 1', FALSE)
				->update('users');
	}

}
?>
