<?php

class Log extends CI_Model {

	var $user_id = '';		// user id
	var $time = '';			// time of redirect
	var $referer = '';		// refering site
	var $ip = '';			// ip of client (will not be shown to users)
	var $agent = '';		// user agent of client (will not be shown to users)

	// Adding log item
	function add($user_id, $referer, $ip, $agent)
	{
		$this->user_id = $user_id;
		$this->time = date('Y-m-d H:i:s');
		$this->referer = $referer;
		$this->ip = $ip;
		$this->agent = $agent;

		$this->db->insert('log', $this);
		return $this->db->insert_id();
	}

	// List log items by user
	function get_by_user($user_id)
	{
		$query = $this->db->where('user_id', $user_id)
				->get('log');
		$result = $query->result_array();
		if (isset($result[0]))
			return $result;
		else
			return FALSE;
	}
}
?>
