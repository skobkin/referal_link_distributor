Install:

0. Get CodeIgniter framework from http://ellislab.com/codeigniter/download
1. Change "$system_path" (CodeIgniter system folder), "$application_folder" and "define('ENVIRONMENT', 'development')" in www/index.php
2. Rename application/config/config.sample.php to application/config/config.php, application/config/database.sample.php to application/config/database.php.
3. Fill it. Don't forget to set $config['encryption_key'] and $config['sess_encrypt_cookie'] in config.php
4. Import database snapshot from https://bitbucket.org/skobkin/referal_link_distributor/downloads
5. Enjoy.